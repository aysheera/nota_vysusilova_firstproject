-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Buy unit in param",
        parameterDefs = {
            { 
                name = "unitName",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "'armbox'",
            },
 	    { 
                name = "count",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "1",
            },
        }
    }
end

function Run(self, units, parameter)
	for i=1, parameter.count do
		message.SendRules({
        		subject = "swampdota_buyUnit",
        		data = {
            			unitName = parameter.unitName
        			},
    			})
	end


    return SUCCESS
end


function Reset(self)
    return self
end