function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			},
			{
				name = "selUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 25

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsDead = Spring.GetUnitIsDead

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	self.threshold = THRESHOLD_DEFAULT
	local position = parameter.position -- Vec3
	local fight = parameter.fight -- boolean
	local selUnits = parameter.selUnits
	

	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	
	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	
	if selUnits == nil then
		return FAILURE
	end

		local success = 0
		for i=1,#selUnits do 
			local currUnit = selUnits[i]
			local pointX, pointY, pointZ = SpringGetUnitPosition(currUnit)
			local currUnitPosition = Vec3(pointX, pointY, pointZ)
			local isCurrUnitDead = SpringGetUnitIsDead(currUnit)
			
			if (isCurrUnitDead ~= true) then
			
				if (currUnitPosition:Distance(position) ~= nill) then
					if (currUnitPosition:Distance(position) < self.threshold) then
						success = 1
					end
				end

				SpringGiveOrderToUnit(currUnit, cmdID, position:AsSpringVector(), {})
				
			end
		end

		if success == 1 then
			return SUCCESS
		else
	
			return RUNNING
		end


end


function Reset(self)
	ClearState(self)
end
