local sensorInfo = {
	name = "splittedGroups",
	desc = "Splits into groups.",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetAllUnits = Spring.GetAllUnits
local Armored = Spring.GetUnitArmored

-- @description return hills
return function(size, filterCagegory)
	local groups = {
		[1] = {},
	}
	local groupIndex = 1
	local maxGroupIndex = 5
	local allUnits = SpringGetAllUnits()
	for i = 1, #allUnits do
		local unitID = allUnits[i]
		local unitDefID = Spring.GetUnitDefID(unitID) 
		local unitDefinition = UnitDefs[unitDefID] -- https://springrts.com/wiki/Lua_UnitDefs
		local unitWeapons = unitDefinition.weapons
		
		-- if it has any weapon
		if 
			unitWeapons ~= nil and
			#unitWeapons > 0
		then
			-- do i need raise group index
			if groupIndex < maxGroupIndex then -- all unit groups with weapons have index 2 or higher
				groupIndex = groupIndex + 1
				if (groups[groupIndex] == nil) then
					groups[groupIndex] = {}
				end
			end
			local thisGroup = groups[groupIndex]
			thisGroup[#thisGroup + 1] = unitID
		else -- it has no weapon
			local thisGroup = groups[1]
			thisGroup[#thisGroup + 1] = unitID		
		end
		--- Spring.Echo(unitDefinition.name)
	end
	
	-- filterCagegory = Categories.nota_vysusilova_firstproject.commander_only
	-- groups[6] = Sensors.core.FilterUnitsByCategory(units, filterCagegory)
	return groups
end

--[[
{
	{
		unitIDTransporter1,
	},
	{
		unitIDrobot1,
	},
	{
		unitIDrobot2,
	}
	{
		unitIDrobot3,
	},
	{
		unitIDrobot4,
		unitIDrobot5,
		unitIDrobot6,
	},
}
]]--