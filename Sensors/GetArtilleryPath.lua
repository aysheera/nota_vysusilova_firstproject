local sensorInfo = {
	name = "GetArtilleryPath",
	desc = "Returns new goals for artillery.",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetLocalTeamID = Spring.GetLocalTeamID
local SpringGetTeamResources = Spring.GetTeamResources

-- @description return hills
return function()
	return Vec3(4445,0,5475)

end
