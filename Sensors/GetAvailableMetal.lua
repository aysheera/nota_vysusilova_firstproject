local sensorInfo = {
	name = "GetAvailableMetal",
	desc = "Returns available metal amount",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetLocalTeamID = Spring.GetLocalTeamID
local SpringGetTeamResources = Spring.GetTeamResources

-- @description return hills
return function()
	local myID = SpringGetLocalTeamID()
	local available,_,_,_,_,_,_,_ = SpringGetTeamResources(myID,"metal")
	return available

end
