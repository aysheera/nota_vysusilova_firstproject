local sensorInfo = {
	name = "Hills",
	desc = "Finds hills.",
	author = "vysusilova",
	date = "2020-06-08",
	license = "notAlicense",
}



local EVAL_PERIOD_DEFAULT = 10000

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


local tileSize = 122
local mapWidthX = Game.mapSizeX
local mapWidthZ = Game.mapSizeZ

local hills = {}
local hillsCount = 1
local springGetGround = Spring.GetGroundHeight


function getHills (hillH)
for x=0,mapWidthX,tileSize
do
	for z=0,mapWidthZ,tileSize
	do
		if springGetGround(x,z) == 192
		then

			hills[hillsCount] = Vec3(x,192,z)
			hillsCount = hillsCount + 1
		end
	end
	
end
end





-- @description return hills
return function()
		hills = {}
		getHills(hillHeight)
		return {
		hills = hills,
			}
end