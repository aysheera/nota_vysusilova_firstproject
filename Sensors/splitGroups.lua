local sensorInfo = {
	name = "splittedGroups",
	desc = "Splits into groups.",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}



local EVAL_PERIOD_DEFAULT = 10000

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local u = Spring.GetAllUnits()
local armored = Spring.GetUnitArmored

function split()
local vel = 4
local groupVel = #units / vel
local groups = {}
local actGroup = 1
for i = 1, #u do
	a = armored(u[i])
	if a then
		groups[actGroup] = u[i]
			
	end
	if actGroup < 4 then
		actGroup = actGroup + 1
	end
		
		
end
end

-- @description return hills
return function()
	groups = {}
	split()
	return {
		groups = groups,
		}
end