local sensorInfo = {
	name = "GetUnitPosition",
	desc = "Returns Vec3 unit position",
	author = "vysusilova",
	date = "2020-09-07",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description returns position
return function(unitID)
	
	local x,y,z = SpringGetUnitPosition(unitID)
	return Vec3(x,y,z)
end
