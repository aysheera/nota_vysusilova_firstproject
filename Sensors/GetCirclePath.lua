local sensorInfo = {
	name = "GetCirclePathNextPoint",
	desc = "Returns a list of direction on the circle with given center and radius",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetUnitPosition = Spring.GetUnitPosition
local sin, cos, rad = math.sin, math.cos, math.rad

-- @description return hills
return function(center,radius)
	local granularity = 5
	local angle = rad(360/granularity)
	local path = {}

	local x = center.x - radius
	local z = center.z + radius
	path[1] = Vec3(x,0,z)

	x = center.x + radius
	z = center.z + radius
	path[2] = Vec3(x,0,z)


	x = center.x - radius
	z = center.z - radius
	path[3] = Vec3(x,0,z)

	x = center.x + radius
	z = center.z - radius
	path[4] = Vec3(x,0,z)
	
	return path
end
