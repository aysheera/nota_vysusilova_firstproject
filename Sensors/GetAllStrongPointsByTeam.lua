local sensorInfo = {
	name = "GetAllStrongpointsByTeam",
	desc = "Returns a list of strongpoints by team",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetLocalTeamID = Spring.GetLocalTeamID

-- @description return hills
return function(points, myTeam)
	local teamID = SpringGetLocalTeamID()
	local strongpoints = {}
	local index = 1
	for i=1, #points do
		local point = points[i]
		if point.isStrongpoint == true then
			if point.ownerAllyID == teamID   then
				if myTeam then
					strongpoints[index] = point
					index = index + 1
				end
			
			else
				if not myTeam then
					strongpoints[index] = point
					index = index + 1
				end
			end
			
			
		end
	end
	return strongpoints

end
