local sensorInfo = {
	name = "GetPossibleBuyCount",
	desc = "How many units can buy",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetLocalTeamID = Spring.GetLocalTeamID
local SpringGetTeamResources = Spring.GetTeamResources

-- @description return hills
return function(name)
	local price = Sensors.core.missionInfo().buy[name]
	local availableMetal = Sensors.nota_vysusilova_firstproject.GetAvailableMetal()
	return math.floor(availableMetal / price)

end
