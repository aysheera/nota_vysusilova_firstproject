local sensorInfo = {
	name = "GetUnitsToSave",
	desc = "Returns list of all units to be saved.",
	author = "vysusilova",
	date = "2020-08-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetAllTeamUnits = Spring.GetAllTeamUnits
local SpringGetUnitDefID  =  SprinGetUnitDefID
local SpringGetTeamUnitsByDefs = Spring.GetTeamUnitsByDefs
local teamID = Spring.GetLocalTeamID()

-- @description return my alive units, which are not in safe area (and are not atlases or scouts)
return function()
	filterDefID = 27
	local allTeamUnitsByType = SpringGetTeamUnitsByDefs(fiterDefID)
	
	
	end

end
