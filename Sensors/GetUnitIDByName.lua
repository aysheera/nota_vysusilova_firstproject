local sensorInfo = {
	name = "GetUnitDefIDByName",
	desc = "Returns def ID of given unit name",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetLocalTeamID = Spring.GetLocalTeamID
local SpringGetTeamResources = Spring.GetTeamResources

-- @description return hills
return function(name)
	for unitDefID, unitDefData in pairs(UnitDefs) do
 		if unitDefData.name == name then
			return unitDefID
  		end
	end
end

