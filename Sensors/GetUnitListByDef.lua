local sensorInfo = {
	name = "GetUnitListByDef",
	desc = "Returns list of units of given definition",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetLocalTeamID = Spring.GetLocalTeamID
local SpringGetTeamResources = Spring.GetTeamResources

-- @description return hills
return function(defID)
	local myID = SpringGetLocalTeamID()
	local result = Spring.GetTeamUnitsByDefs(myID, defID)
	return result

end
