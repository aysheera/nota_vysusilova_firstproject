local sensorInfo = {
	name = "splittedGroups",
	desc = "Splits into groups.",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetAllUnits = Spring.GetAllUnits
local Armored = Spring.GetUnitArmored

-- @description return hills
return function(points)
	for i=1, #points do
		local point = points[i]
		if point.isStrongpoint == true then
			if point.ownerAllyID ~= Spring.GetLocalTeamID() then
				return point
			end
		end
	end

end
