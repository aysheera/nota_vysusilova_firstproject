local sensorInfo = {
	name = "GetNextUnitTobeSaved",
	desc = "Returns next goal for atlas saving",
	author = "vysusilova",
	date = "2020-09-09",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function getDistance(a_x, a_z, b_x, b_z)
	return ((a_x-b_x)^2 + (a_z - b_z)^2)^0.5
end

local SpringGetAllUnits = Spring.GetAllUnits
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return hills
return function(units, safeAreaCenter, safeAreaRadius)
	for i=1, #units do
		local unitID = units[i]
		local unitPosition_x, unitPosition_y, unitPosition_z = SpringGetUnitPosition(unitID)
		local distance = getDistance(safeAreaCenter.x, safeAreaCenter.z, unitPosition_x, unitPosition_z) 

		--Spring.Echo(distance)
		local isNotInSafeArea = distance > safeAreaRadius
		--local isNotInSafeArea = true
		if isNotInSafeArea then
			return unitID
		end
		
	end

end
