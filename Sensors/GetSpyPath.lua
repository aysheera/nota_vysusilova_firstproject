
local sensorInfo = {
	name = "GetSpyPath",
	desc = "Returns path for spies",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetLocalTeamID = Spring.GetLocalTeamID
local SpringGetTeamResources = Spring.GetTeamResources

-- @description return hills
return function()
	local myTeam = SpringGetLocalTeamID()
	local corridors = Sensors.core.MissionInfo().corridors.Middle.points
	local myStrongpoints = Sensors.nota_vysusilova_firstproject.GetAllStrongPointsByTeam(corridors, true)
	local enemyStrongpoints = Sensors.nota_vysusilova_firstproject.GetAllStrongPointsByTeam(corridors, false)
	
	local path = {}
	for i=1, #myStrongpoints do
		path[i] = myStrongpoints[i].position + Vec3(500,0,0)
	end
	path[#path + 1] = enemyStrongpoints[1].position + Vec3(500,0,0)
	
	return path


end