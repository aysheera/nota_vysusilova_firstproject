local sensorInfo = {
	name = "GetFreeUnit",
	desc = "Returns free unit of given type",
	author = "vysusilova",
	date = "2020-09-09",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


local SpringGetAllUnits = Spring.GetAllUnits
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitDefID = Spring.GetUnitDefID

-- @description return hills
return function(units, unitType, freeFunction)
	for i=1, #units do
		local unitID = units[i]
		local unitDefID = SpringGetUnitDefID(unitID)
		local unitDefinition = UnitDefs[unitDefID] 
		--Spring.Echo(distance) do
  		if unitDefinition.name == unitType then
			local isFree = freeFunction(unitID)
			--local isNotInSafeArea = true
			if isFree then
				return unitID
			end
  		end

		
	end

end
