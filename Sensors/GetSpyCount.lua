local sensorInfo = {
	name = "GetSpyCount",
	desc = "Returns how many pewees I have",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetAllUnits = Spring.GetAllUnits


-- @description return hills
return function()
	local myID = Spring.GetLocalTeamID()
	local unitList = Spring.GetTeamUnitsByDefs(myID, 144)
	return #unitList
end
