local sensorInfo = {
	name = "GetCirclePathNextPoint",
	desc = "Returns a list of direction on the circle with given center and radius",
	author = "vysusilova",
	date = "2020-08-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetUnitPosition = Spring.GetUnitPosition
local sin, cos, rad = math.sin, math.cos, math.rad

-- @description return hills
return function(center,radius)
	local granularity = 5
	local angle = rad(360/granularity)

	for i=1,#units do
		local unitID = units[i]
		local x,y,z = SpringGetUnitPosition(unitID)
		if center.x < x then
			if center.z < z then
				x = center.x - radius
				z = center.z + radius
			else
				x = center.x + radius
				z = center.z + radius
			end
		else
			if center.z < z then
				x = center.x - radius
				z = center.z - radius
			else
				x = center.x + radius
				z = center.z - radius
			end
		end
	
	end
	
	return Vec3(x,y,z)
end
